package com.test.swcarcas

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface AppModule {
    @Singleton
    @Binds
    fun provideContext(application: Application): Context
}