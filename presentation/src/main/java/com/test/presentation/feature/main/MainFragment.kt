package com.test.presentation.feature.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.test.presentation.core.ui.BaseBindingFragment
import com.test.swcarcas.presentation.databinding.FragmentMainBinding

class MainFragment : BaseBindingFragment<FragmentMainBinding>() {
    private val viewModel by viewModel<MainViewModel>()
    private val parentViewModel by parentViewModel<MainViewModel>()

    override fun createBinding(layoutInflater: LayoutInflater, container: ViewGroup?): FragmentMainBinding {
        return FragmentMainBinding.inflate(layoutInflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}