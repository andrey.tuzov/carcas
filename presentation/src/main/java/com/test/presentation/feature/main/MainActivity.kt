package com.test.presentation.feature.main

import android.view.LayoutInflater
import com.test.presentation.core.ui.BaseBindingActivity
import com.test.swcarcas.presentation.databinding.ActivityMainBinding

class MainActivity : BaseBindingActivity<ActivityMainBinding>() {
    private val viewModel: MainViewModel by viewModel()

    override fun createBinding(layoutInflater: LayoutInflater): ActivityMainBinding {
        return ActivityMainBinding.inflate(layoutInflater)
    }
}