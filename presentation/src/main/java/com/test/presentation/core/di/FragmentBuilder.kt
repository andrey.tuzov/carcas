package com.test.presentation.core.di

import com.test.common.core.di.FragmentScope
import com.test.presentation.feature.main.MainFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface FragmentBuilder {
    @FragmentScope
    @ContributesAndroidInjector
    fun bindMainFragment(): MainFragment
}