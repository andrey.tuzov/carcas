package com.test.presentation.core.ui

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.test.presentation.core.view_model.ViewModelFactory
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

abstract class BaseFragment : Fragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var viewModelProvider: ViewModelProvider
    lateinit var parentViewModelProvider: ViewModelProvider

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelProvider = ViewModelProvider(viewModelStore, viewModelFactory)
        parentViewModelProvider = ViewModelProvider(requireActivity().viewModelStore, viewModelFactory)
    }

    inline fun <reified VM : ViewModel> viewModel(): Lazy<VM> {
        return lazy { viewModelProvider[VM::class.java] }
    }

    inline fun <reified VM : ViewModel> parentViewModel(): Lazy<VM> {
        return lazy { parentViewModelProvider[VM::class.java] }
    }
}