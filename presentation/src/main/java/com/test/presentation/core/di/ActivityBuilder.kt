package com.test.presentation.core.di

import com.test.common.core.di.ActivityScope
import com.test.presentation.feature.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityBuilder {
    @ActivityScope
    @ContributesAndroidInjector
    fun bindMainActivity(): MainActivity
}