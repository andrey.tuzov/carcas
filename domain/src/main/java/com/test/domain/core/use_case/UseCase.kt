package com.test.domain.core.use_case

interface UseCase<T, in Params> {
    suspend operator fun invoke(params: Params? = null): T
}