package com.test.domain.core.di

import com.test.domain.repository.AuthRepository
import com.test.domain.repository.AuthRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
interface RepositoryModule {
    @Binds
    fun bindAuthRepository(repository: AuthRepositoryImpl): AuthRepository
}