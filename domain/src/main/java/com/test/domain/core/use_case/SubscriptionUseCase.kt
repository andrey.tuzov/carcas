package com.test.domain.core.use_case

import kotlinx.coroutines.flow.Flow

interface SubscriptionUseCase<T, in Params> {
    suspend operator fun invoke(params: Params? = null): Flow<T>
}