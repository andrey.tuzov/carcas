package com.test.data.core.di

import com.test.data.network.NetworkManager
import com.test.data.network.NetworkManagerImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface NetworkModule {
    @Singleton
    @Binds
    fun bindNetworkManager(manager: NetworkManagerImpl): NetworkManager
}